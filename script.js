let rndNum = Math.floor(Math.random() * 10); //it generates random number between 0-9
let winText = document.querySelector(".win-txt"); //it stores the first element with "win-txt" as class


function GuessedNumber()
{
    // it make sures if the random number generated is 0 it will reload the page
    if(rndNum == 0)
    location. reload();
    

    var ans = document.getElementById("num").value; //it stores the element of "num" as its ID

    // this if statement makes sure if the entered value is number, if not it will give a alert message and reload the page
    if (isNaN(ans) || ans < 0 || ans > 9) 
    {
        alert("Pick a number between 0-9 and only number");// this code gives the alert message
        location. reload(); // this code reloads the page
    }

    
// this if statement checks if the entered value and the randomly generated value is correct, if true is will make the heading with "win-txt" appear
    if(rndNum == ans)
    {
        winText.style.display = "block";
    }
}